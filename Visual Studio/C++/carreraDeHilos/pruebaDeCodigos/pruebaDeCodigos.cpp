// pruebaDeCodigos.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include <iostream>
#include <thread>
#include <algorithm>
#include <vector>

using namespace std;

void doTask(int id)
{
	for (int i = 0; i < 100; i++)
	{
		cout << "Hilo:" << id  << " Valor=" << i<<endl;
	}
	cout << "\nHilo:" << id << " TERMINO" << endl;
	
}

int main()
{
	thread hilos[5];
	for (int i = 0; i < 5; i++)
		hilos[i]=thread(doTask, i);
	//for (int i = 0; i < 5;i++)
		//hilos[i].join();
	cin.get();
	return 0;
}