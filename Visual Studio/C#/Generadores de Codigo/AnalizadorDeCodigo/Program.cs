﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AnalizadorDeCodigo
{
    public enum ESTADOS { EsperandoNumero,EscribiendoLineas}

    class Program
    {
        static ControladorCodigoSaliente controladorCodigo;
        static void Main(string[] args)
        {
            string rutaArchivo = args[0];
            string []lineasTextoDeArchivo = File.ReadAllLines(rutaArchivo);
            ESTADOS estadosLectura=ESTADOS.EsperandoNumero;
            int numeroDeLineasSiguientes=0,numeroCasos=0;
            List<Caso> listaCasos = new List<Caso>();
            bool seTermino = false;

            //Por cada linea de texto
            foreach(string lineaTexto in lineasTextoDeArchivo)
            {
                //Si termina por un cero
                if (seTermino)
                    break;

                switch (estadosLectura)
                {
                    case ESTADOS.EsperandoNumero:
                        //Si lo puede convertir en numero
                        if (int.TryParse(lineaTexto, out numeroDeLineasSiguientes))
                        {
                            //Checamos en cuyo caso de que sean cero
                            if (numeroDeLineasSiguientes == 0)
                            {
                                seTermino = true;
                                break;
                            }
                            estadosLectura = ESTADOS.EscribiendoLineas;
                            listaCasos.Add(new Caso(++numeroCasos + ""));
                        }
                        //Si no pudo, es que hay un error
                        else
                        {
                            estadosLectura = ESTADOS.EsperandoNumero;

                        }
                        break;

                    case ESTADOS.EscribiendoLineas:
                         /* Aqui es el analizis*/
                        analizarLinea(lineaTexto,listaCasos.ElementAt(numeroCasos-1));

                        //Checamos de que se haya terminado de leer 
                        if(--numeroDeLineasSiguientes==0)
                        {
                            estadosLectura = ESTADOS.EsperandoNumero;
                        }
                        break;
                }
            }
            foreach(Caso casoActual in listaCasos)
            {
                Console.Write(casoActual.escribirCasoCompleto());
            }
            Console.ReadKey();
        }
        static public void analizarLinea(string lineaTexto,Caso casoActual)
        {
            lineaTexto = lineaTexto.Replace("\"", "\\\"");
            casoActual.listaLineasTexto.Add
                (
                "printf(\"" + lineaTexto + "\\n\");\n"
                );
        }

    }

    class ControladorCodigoSaliente
    {
        public string nombreDeCarpetaResultado;
        public string rutaArchivoResultado;
        public List<string> textoAEscribir = new List<string>();
        public string[] registros = new string[9];

        public ControladorCodigoSaliente()
        {
            nombreDeCarpetaResultado = "Resultado";
            rutaArchivoResultado = nombreDeCarpetaResultado + "/resultado.txt";

            //Se crea la carpeta donde se guarda el archivo de resultado
            if (!Directory.Exists(nombreDeCarpetaResultado))
                Directory.CreateDirectory(nombreDeCarpetaResultado);

        }
        public void escribirError()
        {
            textoAEscribir.Add("ERroR");
        }

        public void escribirCodigoGenerador()
        {
            File.WriteAllLines(rutaArchivoResultado, textoAEscribir.ToArray());
        }

        
    }

    class Caso
    {
        public string cabecera;
        public string piePagina;
        public List<string> listaLineasTexto = new List<string>();

        public Caso(string numeroCaso)
        {
            cabecera ="Case "+numeroCaso+":\n"
            +"#include<string.h>\n"
            + "#include<stdio.h>\n"
            + "int main()\n"
            + "{\n";

            piePagina = "printf(\"\\n\");\n"
            + "return 0;\n"
            + "}\n";
        }

        public string escribirCasoCompleto ()
        {
            string textoCompleto;
            textoCompleto=cabecera;

            foreach(string lineaTexto in listaLineasTexto)
            {
                textoCompleto=textoCompleto+lineaTexto;
            }
            textoCompleto=textoCompleto+piePagina;
            return textoCompleto;
        }
    }
}
