﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Generadores_de_Codigo
{
    public enum ListaSemantica { variable,L,A,S,M,D,R,ST}
    class Nodo
    {
        public string lexema;
        public ListaSemantica semantico;
        public Nodo[] listaHijos=new Nodo[2];

        public Nodo()
        {

        }

        public Nodo(char lexema,ListaSemantica semantico)
        {
            this.lexema=lexema.ToString();
            this.semantico = semantico;
        }
    }

    class Program
    {
        static ControladorCodigoSaliente controladorCodigo = new ControladorCodigoSaliente();

        static void Main(string[] args)
        {
            //Obtener la ruta del archivo
            string rutaArchivo = args[0];
            string textoDeArchivo=File.ReadAllText(rutaArchivo);
            
            //Craemos el arbol
            Nodo nodoRaiz= crearArbolDeNodos(textoDeArchivo);
            
            //Recorremos para escribir 
            RecursivaRaiz(nodoRaiz);
            
            try
            {
                controladorCodigo.escribirCodigoGenerador();
            }
            catch(Exception)
            {
                controladorCodigo.textoAEscribir.Add("Error");
            }
            controladorCodigo.escribirCodigoGenerador();
            foreach(string lineaTexto in controladorCodigo.textoAEscribir)
            {
                Console.WriteLine(lineaTexto);
            }
            Console.WriteLine("<Resultado listo en /Resultado/resultado.txt>");
            Console.ReadKey();

        }        

        static public void RecursivaRaiz(Nodo nodoRaiz)
        {
            if (nodoRaiz !=null)
            {
                //Buscamos el fodo
                RecursivaRaiz(nodoRaiz.listaHijos[0]);
                RecursivaRaiz(nodoRaiz.listaHijos[1]);

                switch(nodoRaiz.semantico)
                {
                    case ListaSemantica.D:
                    case ListaSemantica.L:
                    case ListaSemantica.M:
                    case ListaSemantica.R:
                    case ListaSemantica.S:
                    case ListaSemantica.A:
                        //Si el nodo es de variables
                        if (nodoRaiz.listaHijos[0].semantico == ListaSemantica.variable)
                        {
                            controladorCodigo.escrituraCodigo(nodoRaiz.listaHijos[0].semantico, nodoRaiz.listaHijos[0].lexema);
                            controladorCodigo.escrituraCodigo(nodoRaiz.semantico, nodoRaiz.lexema);
                            controladorCodigo.escrituraCodigo(nodoRaiz.listaHijos[1].semantico, nodoRaiz.listaHijos[1].lexema);
                            controladorCodigo.escrituraCodigo(ListaSemantica.variable, nodoRaiz.lexema);
                        }
                        else
                        {
                            controladorCodigo.removerUltimaLineaTexto();
                            controladorCodigo.escrituraCodigo(nodoRaiz.semantico, nodoRaiz.lexema);
                            controladorCodigo.escrituraCodigo(ListaSemantica.variable, nodoRaiz.listaHijos[0].lexema);
                            controladorCodigo.escrituraCodigo(ListaSemantica.variable, nodoRaiz.listaHijos[0].lexema);
                        }
                        break;
                }
            }
        }

        static public Nodo crearArbolDeNodos(string textoDeArchivo)
        {
            Nodo nodoRaiz=null;
            Nodo raizTemporal=null;
            List<Nodo>listaNodos=new List<Nodo>();
            int numeroSTorages = 0;
            for(int i=0;i<textoDeArchivo.Length;i++)
            {
                //Caracter por caracter
                char caracterActual=textoDeArchivo[i];

                //Segun la letra que llega
                switch (caracterActual)
                {
                    case '-' :
                    case '*' :
                    case '/' :
                    case '%' :
                    case '+' :
                        //Si no es nulo la raiz temporal
                        if(raizTemporal!=null)
                        {
                            raizTemporal.lexema="$"+(++numeroSTorages);

                            switch(caracterActual)
                            {
                                case '+':
                                    raizTemporal.semantico = ListaSemantica.A;
                                    break;
                                case '-':
                                    raizTemporal.semantico = ListaSemantica.S;
                                    break;
                                case '*':
                                    raizTemporal.semantico = ListaSemantica.M;
                                    break;
                                case '/':
                                    raizTemporal.semantico = ListaSemantica.D;
                                    break;
                                case '%':
                                    raizTemporal.semantico = ListaSemantica.R;
                                    break;                                
                            }
                            listaNodos.Add(raizTemporal);
                            raizTemporal=null;
                        }
                            //Es nulo la raiz temporal
                        else
                        {
                            raizTemporal=new Nodo();
                            switch (caracterActual)
                            {
                                case '+':
                                    raizTemporal.semantico = ListaSemantica.A;
                                    break;
                                case '-':
                                    raizTemporal.semantico = ListaSemantica.S;
                                    break;
                                case '*':
                                    raizTemporal.semantico = ListaSemantica.M;
                                    break;
                                case '/':
                                    raizTemporal.semantico = ListaSemantica.D;
                                    break;
                                case '%':
                                    raizTemporal.semantico = ListaSemantica.R;
                                    break;
                            }

                            raizTemporal.listaHijos[1]=listaNodos.Last();
                            listaNodos.RemoveAt(listaNodos.Count-1);
                               
                            raizTemporal.listaHijos[0] = listaNodos.Last();                                
                            raizTemporal.lexema = raizTemporal.listaHijos[0].lexema;
                            listaNodos.RemoveAt(listaNodos.Count - 1);

                            listaNodos.Add(raizTemporal);
                            raizTemporal=null;
                        }
                        break;

                        //Si es una letra variable
                    default:
                        //Si es nulo el raiz temporal
                        if(raizTemporal==null)
                        {
                            raizTemporal=new Nodo();
                            raizTemporal.listaHijos[0]=new Nodo(caracterActual,ListaSemantica.variable);
                        }
                            //Si no es nulo el temporal
                        else
                        {
                            raizTemporal.listaHijos[1] = new Nodo(caracterActual, ListaSemantica.variable);
                        }
                        break;

                }
            }
            nodoRaiz=listaNodos.Last();
            return nodoRaiz;
        }

    }

    class ControladorCodigoSaliente
    {
        public string nombreDeCarpetaResultado;
        public string rutaArchivoResultado;
        public List<string> textoAEscribir = new List<string>();
        public string[] registros = new string[9];
        public EstadosEnsamblador estadoActualEnsamblador;

        public ControladorCodigoSaliente()
        {
            nombreDeCarpetaResultado = "Resultado";
            rutaArchivoResultado = nombreDeCarpetaResultado + "/resultado.txt";  

            //Se crea la carpeta donde se guarda el archivo de resultado
            if(!Directory.Exists(nombreDeCarpetaResultado))
                Directory.CreateDirectory(nombreDeCarpetaResultado);

        }
        public void escribirCodigoGenerador()
        {
            File.WriteAllLines(rutaArchivoResultado, textoAEscribir.ToArray());
        }
        public void removerUltimaLineaTexto()
        {
            textoAEscribir.RemoveAt(textoAEscribir.Count - 1);
        }
        public void escrituraCodigo(ListaSemantica tipoVariable,string valor)
        {
            //Dependidendo de cada estado
            switch (tipoVariable)
            {
                case ListaSemantica.A:
                    estadoActualEnsamblador = EstadosEnsamblador.Add;
                    return;
                case ListaSemantica.S:
                    estadoActualEnsamblador = EstadosEnsamblador.Substract;
                    return;
                case ListaSemantica.M:
                    estadoActualEnsamblador = EstadosEnsamblador.Multiply;
                    return;
                case ListaSemantica.D:
                    estadoActualEnsamblador = EstadosEnsamblador.Divide;
                    return;
                case ListaSemantica.R:
                    estadoActualEnsamblador = EstadosEnsamblador.Remainder;
                    return;
            }

            switch(estadoActualEnsamblador)
            {
                case EstadosEnsamblador.Load:
                    textoAEscribir.Add("L " + valor);
                    break;
                case EstadosEnsamblador.STorage:
                    textoAEscribir.Add("ST " + valor);
                    estadoActualEnsamblador = EstadosEnsamblador.Load;
                    break;
                case EstadosEnsamblador.Add:
                    textoAEscribir.Add("A " + valor);
                    estadoActualEnsamblador = EstadosEnsamblador.STorage;
                    break;
                case EstadosEnsamblador.Substract:
                    textoAEscribir.Add("S " + valor);
                    estadoActualEnsamblador = EstadosEnsamblador.STorage;
                    break;
                case EstadosEnsamblador.Multiply:
                    textoAEscribir.Add("M " + valor);
                    estadoActualEnsamblador = EstadosEnsamblador.STorage;
                    break;
                case EstadosEnsamblador.Divide:
                    textoAEscribir.Add("D " + valor);
                    estadoActualEnsamblador = EstadosEnsamblador.STorage;
                    break;
                case EstadosEnsamblador.Remainder:
                    textoAEscribir.Add("R " + valor);
                    estadoActualEnsamblador = EstadosEnsamblador.STorage;
                    break;
            }
        }        
    }

    public enum EstadosEnsamblador
    {
        Load,Add,Substract,Multiply,Divide,Remainder,STorage
    }
}
