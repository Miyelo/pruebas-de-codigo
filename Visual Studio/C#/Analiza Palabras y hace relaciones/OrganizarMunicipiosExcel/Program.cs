﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace OrganizarMunicipiosExcel
{
    class Program
    {
        static void Main(string[] args)
        {
            List<LineaMunicipioExcel> listaMunicipiosExcel = new List<LineaMunicipioExcel>();
            List<LineaMunicipioExcel> listaOrganizadaMunicipios = new List<LineaMunicipioExcel>();
            List<EstructuraEstado> listaEstados = new List<EstructuraEstado>();
            List<string> lineasTextoAImprimir = new List<string>();
            //Cargamos los datos a las listas
            //Primero los Distritos
            string[] lineasDeTexto = File.ReadAllLines("relacionMunicipiosDistritoExcel.txt", Encoding.UTF8);
            foreach (string lineaDeTexto in lineasDeTexto)
            {
                string[] lineaSeparada = lineaDeTexto.Split(',');
                string nombreEstado = lineaSeparada[0],
                    numeroDistrito = lineaSeparada[1],
                    nombreDistrito = lineaSeparada[2],
                    nombreMunicipio=lineaSeparada[3];
                listaMunicipiosExcel.Add(new LineaMunicipioExcel(nombreEstado, numeroDistrito,nombreDistrito,nombreMunicipio));
            }

            string nombreEstadoAnterior = "";
            EstructuraEstado auxEstado=new EstructuraEstado();

            //por cada miembro generamos nuestra lista de estados
            foreach(LineaMunicipioExcel lineaMunicipio in listaMunicipiosExcel)
            {
                //Si no es el mismo estado anterior
                if (nombreEstadoAnterior != lineaMunicipio.nombreEstado)
                {
                    if (nombreEstadoAnterior == "")
                    {
                        nombreEstadoAnterior = lineaMunicipio.nombreEstado;
                        
                    }
                    if(nombreEstadoAnterior!=lineaMunicipio.nombreEstado)
                    {
                        listaEstados.Add(auxEstado);
                    }

                    auxEstado = new EstructuraEstado(lineaMunicipio.nombreEstado);
                    auxEstado.listaMunicipios.Add(new EstructuraMunicipio(lineaMunicipio.numeroDistrito,
                        lineaMunicipio.nombreDistrito, lineaMunicipio.nombreMunicipio));
                    nombreEstadoAnterior = lineaMunicipio.nombreEstado;
                }
                    //Si es el mismo que anterior
                else
                {                    
                    auxEstado.listaMunicipios.Add(new EstructuraMunicipio(lineaMunicipio.numeroDistrito, 
                        lineaMunicipio.nombreDistrito, lineaMunicipio.nombreMunicipio));

                    //if(nombreEstadoAnterior)
                }
            }
            //Agregamos el ultimo estado, se colo
            listaEstados.Add(auxEstado);
            
            /*de aqui ya tenemos los nombres de estados*/
            //Por cada estado, ver cada entrada
            
            int contadorId = 0; 
            string textoActualIdDistrito =contadorId+"";
            foreach(EstructuraEstado estado in listaEstados)            
            {
                //Por cada municipio de cada estado
                foreach(EstructuraMunicipio municipio in estado.listaMunicipios)
                {   
                    //Si el mismo caracter que teniamos antes
                    if(municipio.numeroDistrito==textoActualIdDistrito)
                    {
                        //Imprime cadena
                        if(municipio.nombreMunicipio!="")
                        {
                            lineasTextoAImprimir.Add(
                            estado.nombreEstado + "," + contadorId + "," + municipio.nombreDistrito + "," + municipio.nombreMunicipio
                            );
                        }
                        
                    }
                        //Si no igual a la anterior
                    else
                    {
                        //Aumentamos el contador de id                        
                        //Imprime cadena
                        if(municipio.nombreMunicipio!="")
                        {
                            lineasTextoAImprimir.Add(
                            estado.nombreEstado + "," + (++contadorId) + "," + municipio.nombreDistrito + "," + municipio.nombreMunicipio
                            );
                        }
                        textoActualIdDistrito = municipio.numeroDistrito;
                    }
                }
            }

            File.WriteAllLines("nuevoArchivoExcel.txt", lineasTextoAImprimir.ToArray());


            //Ahora checar cada lista de estado, y ordenarla
            Console.ReadKey();
        }

        
    }

    struct EstructuraEstado
    {
        public string nombreEstado;
        public List<EstructuraMunicipio> listaMunicipios;

        public EstructuraEstado(string nombreEstado)
        {
            this.nombreEstado = nombreEstado;
            this.listaMunicipios=new List<EstructuraMunicipio>();
        }
    }

    struct EstructuraMunicipio
    {
        public string numeroDistrito,nombreDistrito,nombreMunicipio;

        public EstructuraMunicipio(string numeroDistrito, string nombreDistrito, string nombreMunicipio)
        {
            this.numeroDistrito = numeroDistrito;
            this.nombreDistrito = nombreDistrito;
            this.nombreMunicipio = nombreMunicipio;
        }
    }

    struct LineaMunicipioExcel
    {
        public string nombreEstado, numeroDistrito, nombreDistrito, nombreMunicipio;

        public LineaMunicipioExcel(string estado,string numeroDistrito,string nombreDistrio,string municipio)
        {
            this.nombreEstado = estado;
            this.numeroDistrito = numeroDistrito;
            this.nombreDistrito = nombreDistrio;
            this.nombreMunicipio = municipio;
        }
    }
}
