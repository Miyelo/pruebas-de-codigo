﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SegundaVuelta
{
    public partial class Form1 : Form
    {
        List<RelacionDistritoMunicipio> listaMunicipiosConRelacion = new List<RelacionDistritoMunicipio>();
        List<MunicipioNoRelacionado> listaMunicipiosSinRealacion = new List<MunicipioNoRelacionado>();
        List<NombreDistritoConIdDistrito> listaDistritos = new List<NombreDistritoConIdDistrito>();

        public Form1()
        {
            InitializeComponent();
            //Cargamos los datos a las listas
            //Primero los Distritos
            string[] lineasDeTexto = File.ReadAllLines("resultado_MunicipiosRestantes.txt", Encoding.UTF8);
            foreach(string lineaDeTexto in lineasDeTexto)
            {
                string[] lineaSeparada = lineaDeTexto.Split(',');
                string idDistrio = lineaSeparada[0],
                    nombreDistrito = lineaSeparada[1];
                listaDistritos.Add(new NombreDistritoConIdDistrito(idDistrio, nombreDistrito));            
            }

            //Ahora los municipiso que faltan
            lineasDeTexto = File.ReadAllLines("resultado_MunicipiosSinRelacion.txt", Encoding.UTF8);
            foreach (string lineaDeTexto in lineasDeTexto)
            {
                string[] lineaSeparada = lineaDeTexto.Split(',');
                string idMunicipio = lineaSeparada[0],
                    nombreMunicipio = lineaSeparada[2];
                listaMunicipiosSinRealacion.Add(new MunicipioNoRelacionado(idMunicipio, nombreMunicipio));
            }
            this.dataGridView1.DataSource = listaDistritos;
            this.dataGridView2.DataSource = listaMunicipiosSinRealacion;
            
            

        }
    }



    class RelacionDistritoMunicipio
    {
        public string idDistrito {get; set;}
        public string idMunicipio{get;set;}
        public string nombreMunicipio { get; set; }

        public RelacionDistritoMunicipio(string idDistrito, string idMunicipio, string nombreMunicipio)
        {
            this.idDistrito = idDistrito;
            this.idMunicipio = idMunicipio;
            this.nombreMunicipio = nombreMunicipio;
        }
    }

    class MunicipioNoRelacionado
    {
        public string idMunicipio{get;set;}
        public string nombreMunicipio{get;set;}

        public string idMunicipioAsignar { get; set; }

        public MunicipioNoRelacionado(string idMunicipio, string nombreMunicipio)
        {
            this.idMunicipio = idMunicipio;
            this.nombreMunicipio = nombreMunicipio;
        }

    }
    class NombreDistritoConIdDistrito
    {
        public string idDistritoAsignado{get;set;}
        public string nombreDisrtito { get; set; }

        public NombreDistritoConIdDistrito(string idDistrito, string nombreDisrtito)
        {
            this.idDistritoAsignado = idDistrito;
            this.nombreDisrtito = nombreDisrtito;
        }
    }

}
