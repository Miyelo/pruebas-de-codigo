﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ComponentModel;

namespace miniProgramaIdMunicipio
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MunicipioBD> listaMunicipiosBD=new List<MunicipioBD>();
            List<MunicipioExcel> listaMunicipiosExcel = new List<MunicipioExcel>();
            List<MunicipioExcel> listaMunicipiosSeleccionados = new List<MunicipioExcel>();
            List<MunicipioExcel> listaMunicipiosRestantes = new List<MunicipioExcel>();
            List<RelacionDistritoMunicipio> listaRelacion = new List<RelacionDistritoMunicipio>();

            BackgroundWorker hilo = new BackgroundWorker();
            hilo.DoWork += hilo_DoWork;
            hilo.RunWorkerAsync();

            Console.WriteLine("Iniciado programa");

            //Sacamos los datos de cada lista
            //listaMunicipiosBD
            #region
            string[] lineasTexto = File.ReadAllLines("resultado_MunicipiosSinRelacion.txt", Encoding.UTF8);
            foreach(string lineaTexto in lineasTexto)
            {
                string[] lineaSeparada = lineaTexto.Split(',');

                //idDistritoAsignado
                string id = lineaSeparada[0],
                    nombre = lineaSeparada[2],
                    idEstado=lineaSeparada[3];
                listaMunicipiosBD.Add(new MunicipioBD(id, nombre,idEstado));
            }
            #endregion

            //Lista Municipios por distrito
            #region
            lineasTexto = File.ReadAllLines("resultado_MunicipiosRestantes.txt");
            foreach (string lineaTexto in lineasTexto)
            {
                string[] lineaSeparada = lineaTexto.Split(',');

                //idDistritoAsignado
                string idDistrito = lineaSeparada[1],
                    nombreDistrito = lineaSeparada[2],
                    nombreMunicipio = lineaSeparada[3],
                    nombreEstado=lineaSeparada[0] ;

                listaMunicipiosExcel.Add(new MunicipioExcel(idDistrito,nombreMunicipio,nombreEstado,nombreDistrito));
            }
            #endregion

            /**/
             
            //Por cada municipio en idDistrito (mayusculas)
            foreach(MunicipioExcel municipioExcel in listaMunicipiosExcel)
            {
                //Checamos validacion de municipioExcel
                //Salio un municipio sin datos
                if (municipioExcel.nombreMunicipio == "")
                    continue;
                //Si el municipio de exel es 0
                if (municipioExcel.nombreMunicipio == "0")
                    continue;

                //Primero si encontramos un solo registro como esta escrito
                bool busquedaExaustiva = false;

                //Busqueda Exaustiva
                #region
                foreach (MunicipioBD municpioBD in listaMunicipiosBD)
                {
                    //Si el nombreMunicipio coincide tal cual con un nombreMunicipio en la lista de BD
                    if(municipioExcel.nombreMunicipio==municpioBD.nombreMunicipio.ToUpper())
                    {
                        listaRelacion.Add(new RelacionDistritoMunicipio(municipioExcel.idDistritoAsignado, municpioBD.idMunicipio, municpioBD.nombreMunicipio, municipioExcel.nombreEstadoAsociado));
                        listaMunicipiosSeleccionados.Add(municipioExcel);
                        listaMunicipiosBD.Remove(municpioBD);
                        busquedaExaustiva = true;
                        break;
                    }
                }
                #endregion

                //si encontro en exaustiva, seguimos con la siguiente palabra de excel
                if (busquedaExaustiva)
                    continue;

                //Si no, ahora toca analizar letra por letra por ocurrencia
                #region
                string palabraGenerandose = "";
                int indicePalabra=0;
                bool soloUnaOcurrencia = false;

                //Mientras no encuentre
                while(!soloUnaOcurrencia)
                {
                    //Agregamos una letra a la ves, buscando una ocurrencia
                    int numeroOcurrencias = 0;

                    //Ingresamos una letra mas al texto
                    try
                    {
                        palabraGenerandose += municipioExcel.nombreMunicipio[indicePalabra++];
                    }   
                        //Cualquier exception
                    catch(Exception)
                    {
                        //Este estado no esta en bd?
                        soloUnaOcurrencia = true;
                        continue;
                    }

                    //Buscamos solo una ocurrencia
                    MunicipioBD municipioSeleccionado=new MunicipioBD();
                    //Por cada municipio en lista de id_municpio
                    foreach(MunicipioBD municipioBD in listaMunicipiosBD)
                    {
                        //Si la palabra contine la palabra que se esta generando
                        if(municipioBD.nombreMunicipio.ToUpper().Contains(palabraGenerandose))
                        {
                            numeroOcurrencias++;
                            municipioSeleccionado = municipioBD;
                        }
                    }

                    //Posibles resultados de numeroOcurrencias                    
                    //En caso de que es cero,
                    if (numeroOcurrencias==0)
                    {
                        //no hay ningun municipio en la bd que tenga este texto
                        //Continuamos con el siguiente municipio de excel

                        //Cuando es cero, jugaremos con los acentos en el caracter anterior
                        //Para que juegue debe ser mayor a cero
                        if (palabraGenerandose.Length > 0)
                        {
                            switch (palabraGenerandose[palabraGenerandose.Length - 1])
                            {
                                case 'A':
                                    palabraGenerandose = palabraGenerandose.Substring(0, palabraGenerandose.Length - 1) + "Á";
                                    numeroOcurrencias = 0;
                                    foreach (MunicipioBD municipioBD in listaMunicipiosBD)
                                    {
                                        //Si la palabra contine la palabra que se esta generando
                                        if (municipioBD.nombreMunicipio.ToUpper().Contains(palabraGenerandose))
                                        {
                                            numeroOcurrencias++;
                                            municipioSeleccionado = municipioBD;
                                        }
                                    }
                                    //Si vuelve hacer cero, el numero de ocurrencias, cortamos busqueda
                                    if (numeroOcurrencias == 0)
                                    {
                                        soloUnaOcurrencia = true;
                                        break;
                                    }
                                    break;
                                case 'E':
                                    palabraGenerandose = palabraGenerandose.Substring(0, palabraGenerandose.Length - 1) + "É";
                                    numeroOcurrencias = 0;
                                    foreach (MunicipioBD municipioBD in listaMunicipiosBD)
                                    {
                                        //Si la palabra contine la palabra que se esta generando
                                        if (municipioBD.nombreMunicipio.ToUpper().Contains(palabraGenerandose))
                                        {
                                            numeroOcurrencias++;
                                            municipioSeleccionado = municipioBD;
                                        }
                                    }
                                    //Si vuelve hacer cero, el numero de ocurrencias, cortamos busqueda
                                    if (numeroOcurrencias == 0)
                                    {
                                        soloUnaOcurrencia = true;
                                        break;
                                    }
                                    break;
                                case 'I':
                                    palabraGenerandose = palabraGenerandose.Substring(0, palabraGenerandose.Length - 1) + "Í";
                                    numeroOcurrencias = 0;
                                    foreach (MunicipioBD municipioBD in listaMunicipiosBD)
                                    {
                                        //Si la palabra contine la palabra que se esta generando
                                        if (municipioBD.nombreMunicipio.ToUpper().Contains(palabraGenerandose))
                                        {
                                            numeroOcurrencias++;
                                            municipioSeleccionado = municipioBD;
                                        }
                                    }
                                    //Si vuelve hacer cero, el numero de ocurrencias, cortamos busqueda
                                    if (numeroOcurrencias == 0)
                                    {
                                        soloUnaOcurrencia = true;
                                        break;
                                    }
                                    break;
                                case 'O':
                                    palabraGenerandose = palabraGenerandose.Substring(0, palabraGenerandose.Length - 1) + "Ó";
                                    numeroOcurrencias = 0;
                                    foreach (MunicipioBD municipioBD in listaMunicipiosBD)
                                    {
                                        //Si la palabra contine la palabra que se esta generando
                                        if (municipioBD.nombreMunicipio.ToUpper().Contains(palabraGenerandose))
                                        {
                                            numeroOcurrencias++;
                                            municipioSeleccionado = municipioBD;
                                        }
                                    }
                                    //Si vuelve hacer cero, el numero de ocurrencias, cortamos busqueda
                                    if (numeroOcurrencias == 0)
                                    {
                                        soloUnaOcurrencia = true;
                                        break;
                                    }
                                    break;
                                case 'U':
                                    palabraGenerandose = palabraGenerandose.Substring(0, palabraGenerandose.Length - 1) + "Ú";
                                    numeroOcurrencias = 0;
                                    foreach (MunicipioBD municipioBD in listaMunicipiosBD)
                                    {
                                        //Si la palabra contine la palabra que se esta generando
                                        if (municipioBD.nombreMunicipio.ToUpper().Contains(palabraGenerandose))
                                        {
                                            numeroOcurrencias++;
                                            municipioSeleccionado = municipioBD;
                                        }
                                    }
                                    //Si vuelve hacer cero, el numero de ocurrencias, cortamos busqueda
                                    if (numeroOcurrencias == 0)
                                    {
                                        soloUnaOcurrencia = true;
                                        break;
                                    }
                                    break;

                            }
                        }
                        //Si es menor no juega
                        else
                        {
                            soloUnaOcurrencia = true;
                            continue;
                        }
                    }
                    
                    //En caso de que sea uno
                    if(numeroOcurrencias==1)
                    {
                        //Es el ideal, ya que nos dice que solo es la unica ocurrencia
                        if (palabraGenerandose == municipioSeleccionado.nombreMunicipio.ToUpper())
                        {
                            listaRelacion.Add(new RelacionDistritoMunicipio(municipioExcel.idDistritoAsignado, municipioSeleccionado.idMunicipio, municipioSeleccionado.nombreMunicipio,municipioExcel.nombreEstadoAsociado));
                            listaMunicipiosSeleccionados.Add(municipioExcel);
                            listaMunicipiosBD.Remove(municipioSeleccionado);
                            soloUnaOcurrencia = true;
                        }

                    }

                    //Si es mayor uno, continuamos el ciclo
                    //Seguimos con el ciclo
                }
                #endregion


            }
             /**/

            //Final imprmir la lista de relacion
            //Relacionados
            List<string> lineaTextoEscrituraArchivo = new List<string>();
            foreach(RelacionDistritoMunicipio relacionMunicipioDistritoMunicipio in listaRelacion)
            {
                string lineaAEscribir = relacionMunicipioDistritoMunicipio.idDistrito + ","
                    + relacionMunicipioDistritoMunicipio.idMunicipio + "," 
                    + relacionMunicipioDistritoMunicipio.nombreMunicipio+","
                    +relacionMunicipioDistritoMunicipio.nombreEstado;
                lineaTextoEscrituraArchivo.Add(lineaAEscribir);
            }
            File.WriteAllLines("resultado_Relacionados.txt", lineaTextoEscrituraArchivo.ToArray(), Encoding.UTF8);

            //No Relacionados de los obtenidos de la bd
            lineaTextoEscrituraArchivo.Clear();
            foreach (MunicipioBD municipioBD in listaMunicipiosBD)
            {
                string lineaAEscribir = municipioBD.idMunicipio + ",0,"
                    + municipioBD.nombreMunicipio + ","
                    + municipioBD.idEstado;
                lineaTextoEscrituraArchivo.Add(lineaAEscribir);
            }
            File.WriteAllLines("resultado_MunicipiosSinRelacion.txt", lineaTextoEscrituraArchivo.ToArray(), Encoding.UTF8);

            //Restantes
            //Primero quitamos los seleccionados del excel
            foreach (MunicipioExcel municipioExcelSeleccionado in listaMunicipiosSeleccionados)
            {
                listaMunicipiosExcel.Remove(municipioExcelSeleccionado);
            }

            lineaTextoEscrituraArchivo.Clear();

            //creamos los municipios de excel que no se seleccionaron
            foreach (MunicipioExcel municipioExvcelRestante in listaMunicipiosExcel)
            {
                string lineaAEscribir = municipioExvcelRestante.nombreEstadoAsociado + ","
                    + municipioExvcelRestante.idDistritoAsignado + ","
                    + municipioExvcelRestante.nombreDistrito+","
                    + municipioExvcelRestante.nombreMunicipio;
                lineaTextoEscrituraArchivo.Add(lineaAEscribir);
            }
            File.WriteAllLines("resultado_MunicipiosRestantes.txt", lineaTextoEscrituraArchivo.ToArray(),Encoding.UTF8);
            
            hilo.Dispose();
            Console.ReadLine();
            Environment.Exit(Environment.ExitCode);
        }

        static void hilo_DoWork(object sender, DoWorkEventArgs e)
        {
            Console.WriteLine("*******************************hilo iniciado");
            int contadorDePuntos = 1;

            while(true)
            {
                switch(contadorDePuntos++)
                {
                    case 1:
                        Console.WriteLine(".");
                        break;
                    case 2:
                        Console.WriteLine("..");
                        break;
                    case 3:
                        Console.WriteLine("...");
                        break;
                    case 4:
                        Console.WriteLine("....");
                        contadorDePuntos = 1;
                        break;
                }                
            }
        }
    }

    struct RelacionDistritoMunicipio
    {
        public string idDistrito, idMunicipio, nombreMunicipio,nombreEstado;

        public RelacionDistritoMunicipio(string idDistrito,string idMunicipio, string nombreMunicipio,string nombreEstado)
        {
            this.idDistrito = idDistrito;
            this.idMunicipio = idMunicipio;
            this.nombreMunicipio = nombreMunicipio;
            this.nombreEstado = nombreEstado;
        }
    }

    struct MunicipioBD
    {
        public string idMunicipio, nombreMunicipio,idEstado;

        public MunicipioBD(string id,string nombre,string idEstado)
        {
            this.idMunicipio = id;
            this.nombreMunicipio = nombre;
            this.idEstado = idEstado;
        }
        
    }
    struct MunicipioExcel
    {
        public string idDistritoAsignado, nombreMunicipio,nombreEstadoAsociado,nombreDistrito;

        public MunicipioExcel(string id, string nombreMunicipio,string nombreEstado,string nombreDistrito)
        {
            this.idDistritoAsignado = id;
            this.nombreMunicipio = nombreMunicipio;
            this.nombreEstadoAsociado = nombreEstado;
            this.nombreDistrito = nombreDistrito;
        }
    }
}
