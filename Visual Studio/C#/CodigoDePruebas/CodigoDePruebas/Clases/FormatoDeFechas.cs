﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodigoDePruebas.Clases
{
    class FormatoDeFechas
    {
        public FormatoDeFechas()
        {
            DateTime tiempoAhora = DateTime.Now;
            Console.WriteLine("formato >" + tiempoAhora.ToString("H:mm:ss MM-dd-yyyy"));
            Console.WriteLine("formato >" + tiempoAhora.ToString("yyyy-mm-dd H:mm:ss"));
        }

    }
}
