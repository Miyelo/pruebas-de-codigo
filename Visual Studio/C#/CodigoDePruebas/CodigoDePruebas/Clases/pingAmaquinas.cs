﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.NetworkInformation;

namespace CodigoDePruebas.Clases
{
    class pingAmaquinas
    {
        public pingAmaquinas()
        {
            Ping ping = new Ping();
            for (int i = 0; i < 4; i++)
            {
                try
                {
                    PingReply pr = ping.Send("holis-lap");
                    Console.WriteLine("Respuesta desde" + pr.Address + " : bytes:" + pr.Buffer + " tiempo=" + pr.RoundtripTime + " (" + pr.Status.ToString() + ")");
                }
                catch
                {
                    Console.WriteLine("Servidor inaccesible");
                }
            }
            Console.ReadLine();
        }        
    }
}
