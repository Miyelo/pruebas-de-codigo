﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CodigoDePruebas.Clases
{
    class ObtenerListaDeArchivosGuardalosEnTXT
    {
        public ObtenerListaDeArchivosGuardalosEnTXT()
        {   
            string rutaCarpeta="C:/Users/Miyelo/Google Drive/Proyecto Moviles/Proyectos desde Mac/ListaImagenes/ListaImagenes/Imagenes";
            string textoListaArchivos = "";
            DirectoryInfo carpeta = new DirectoryInfo(rutaCarpeta);
            FileInfo[] listaArchivos = carpeta.GetFiles();

            foreach(FileInfo archivo in listaArchivos)
            {
                textoListaArchivos += archivo.FullName+"\n";
            }
            File.WriteAllText(rutaCarpeta + "/ListaDeImagenes.txt", textoListaArchivos);
        }
    }
}
