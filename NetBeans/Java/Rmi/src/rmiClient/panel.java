/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package rmiClient;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

/**
 *
 * @author ING. FERNANDO
 */
public class panel extends javax.swing.JPanel
{
    BufferedImage _image;

    //Creamos nuestro constructor
    public panel(BufferedImage image,Dimension d){
        this._image=image;
        this.setSize(d);        
    }

    @Override

    public void paint(Graphics g){

        ImageIcon imagenFondo = new ImageIcon(_image);//Capturamos la imagen
        g.drawImage(imagenFondo.getImage(),0,0,getWidth(),getHeight(), null);//Asignacion de tamaño de imagen
        setOpaque(false);
        super.paintComponent(g);

    }




}
