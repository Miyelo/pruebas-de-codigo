package rmiClient;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ING. FERNANDO
 */
public class metodos {

    public BufferedImage _image = null;   //Capturar la imagen
    public BufferedImage imagenOriginal=null;
    private FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivo de Imagen","jpg","png");//Extension de imagen

   //Esta funcion nos permitira abrir y mostrar la en el panel
    public void Abrir_Dialogo(JPanel p)
    {
       JFileChooser fileChooser = new JFileChooser();
       fileChooser.setFileFilter(filter);
       int result = fileChooser.showOpenDialog(null);
       if ( result == JFileChooser.APPROVE_OPTION )
       {
            try 
            {
                //se asigna a "url" el archivo de imagen seleccionado
                URL url = fileChooser.getSelectedFile().toURL();
                //se lo coloca en memoria
                cargar_imagen_en_buffer(url);
                imagenOriginal=_image;
                //se aÃ±ade al contenedor
                agregarImagenAlPanel(p, _image);
            }
            catch (IOException ex) 
            {
                Logger.getLogger(metodos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public BufferedImage Obtener_imagen_de_Buffer()
    {
        return _image;
    }

    public void cargar_imagen_en_buffer(URL _url) throws IOException
    {        
        _image=ImageIO.read(_url);        
    }
    
    public BufferedImage obtenerImagenOriginal()
    {
        return imagenOriginal;
    }
    
    public void agregarImagenAlPanel(JPanel p,BufferedImage imagen)
    {
        p.removeAll();
        p.add(new panel(imagen, p.getSize()));
        p.setVisible(true);
        p.repaint();
    }

}
