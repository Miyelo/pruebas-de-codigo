/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmiServer;
import Inteface.*;
import java.io.Serializable;
import java.rmi.Remote;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.rmi.*;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.server.*;
import javax.imageio.ImageIO;
/**
 *
 * @author Miyelo
 */
public class servidor extends UnicastRemoteObject implements RmiInterface
{   
    BufferedImage imagen=null;
    
    public servidor() throws RemoteException
    {
        
    }
    
    @Override
    public byte[] convertirBlancoYNego(byte[] imagenAProcesar) 
    {
        System.out.println("Entro a Escala de Grises");
        try
        {
        this.imagen=ImageIO.read(new ByteArrayInputStream(imagenAProcesar));
        }
        catch (IOException e)
        {
            return null;
        }
        
        int width=imagen.getWidth();
        int height = imagen.getHeight();
        
        //convert to grayscale
        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                int p = imagen.getRGB(x,y);                
                int a = (p>>24)&0xff;
                int r = (p>>16)&0xff;
                int g = (p>>8)&0xff;
                int b = p&0xff;
                
                //calculate average
                int avg = (r+g+b)/3;
                
                //replace RGB value with avg
                p = (a<<24) | (avg<<16) | (avg<<8) | avg;
                
                imagen.setRGB(x, y, p);
            }
        }
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        try
        {
            ImageIO.write(imagen,"png",baos);
            return baos.toByteArray();
        }
        catch (IOException e)
        {
            return null;
        }        
    }
    
    public static void main(String[] args) 
    {
        try
        {
            servidor server=new servidor();
            Registry registro= LocateRegistry.createRegistry(7777);
            registro.rebind("rmi:Remoto", server);
            while(true)
            {
                
            }
        }
        catch(RemoteException e)
        {
            System.out.println("Error en Main,Main_Server, main"+ e.getMessage());
        }
    }

    @Override
    public byte[] convertirRojo(byte[] imagenAProcesar) throws RemoteException 
    {
        System.out.println("Entro a Escala de Rojo");
        try
        {
        this.imagen=ImageIO.read(new ByteArrayInputStream(imagenAProcesar));
        }
        catch (IOException e)
        {
            return null;
        }
        
        int width=imagen.getWidth();
        int height = imagen.getHeight();
        
        //convert to grayscale
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                int p = imagen.getRGB(x,y);
                
                int a = (p>>24)&0xff;
                int r = (p>>16)&0xff;
                
                //set new RGB
                p = (a<<24) | (r<<16) | (0<<8) | 0;
                
                imagen.setRGB(x, y, p);
            }
        }
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        try
        {
            ImageIO.write(imagen,"png",baos);
            return baos.toByteArray();
        }
        catch (IOException e)
        {
            return null;
        }
    }

    @Override
    public byte[] convertirVerde(byte[] imagenAProcesar) throws RemoteException {
        System.out.println("Entro a Escala de Verde");
        try
        {
        this.imagen=ImageIO.read(new ByteArrayInputStream(imagenAProcesar));
        }
        catch (IOException e)
        {
            return null;
        }
        
        int width=imagen.getWidth();
        int height = imagen.getHeight();
        
        //convert to grayscale
        //convert to green image
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                int p = imagen.getRGB(x,y);
                
                int a = (p>>24)&0xff;
                int g = (p>>8)&0xff;
                
                //set new RGB
                p = (a<<24) | (0<<16) | (g<<8) | 0;
                
                imagen.setRGB(x, y, p);
            }
        }
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        try
        {
            ImageIO.write(imagen,"png",baos);
            return baos.toByteArray();
        }
        catch (IOException e)
        {
            return null;
        }
    }

    @Override
    public byte[] convertirAzul(byte[] imagenAProcesar) throws RemoteException {
        System.out.println("Entro a Escala de Azul");
        try
        {
        this.imagen=ImageIO.read(new ByteArrayInputStream(imagenAProcesar));
        }
        catch (IOException e)
        {
            return null;
        }
        
        int width=imagen.getWidth();
        int height = imagen.getHeight();
        
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                int p = imagen.getRGB(x,y);
                
                int a = (p>>24)&0xff;
                int b = p&0xff;
                
                //set new RGB
                p = (a<<24) | (0<<16) | (0<<8) | b;
                
                imagen.setRGB(x, y, p);
            }
        }
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        try
        {
            ImageIO.write(imagen,"png",baos);
            return baos.toByteArray();
        }
        catch (IOException e)
        {
            return null;
        }
    }
    
}
