/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inteface;
import java.rmi.Remote;
import java.io.File;
import java.io.IOException;
import java.awt.image.*;
import java.rmi.RemoteException;
import javax.imageio.ImageIO;

/**
 *
 * @author Miyelo
 */
public interface RmiInterface extends Remote
{
    public abstract byte[] convertirBlancoYNego(byte []imagenAProcesar)throws RemoteException;    
    public abstract byte[] convertirRojo(byte []imagenAProcesar)throws RemoteException;    
    public abstract byte[] convertirVerde(byte []imagenAProcesar)throws RemoteException;    
    public abstract byte[] convertirAzul(byte []imagenAProcesar)throws RemoteException;    
}

