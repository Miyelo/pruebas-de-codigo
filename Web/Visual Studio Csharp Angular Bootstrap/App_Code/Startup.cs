﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Visual_Studio_Csharp_Angular_Bootstrap.Startup))]
namespace Visual_Studio_Csharp_Angular_Bootstrap
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
