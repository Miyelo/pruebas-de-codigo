		<table id="0_3" class="hiddenDiv" onclick="showAnswer(this.id,100);" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="middle"><p class="preguntas">
		Una porción es una igualdad de dos
		<br>
		<br>
		a) Adiciones<br><br>	b)Razones<br><br>	c)Sustracciones<br><br></p></td></tr></tbody></table>

		<table id="0_3_resp" class="hiddenDiv" onclick="showScore(this.id)" border="0" cellpadding="0" cellspacing="0" width="100%" ><tbody><tr><td align="center" valign="middle"><p class="respuestas">Razones</p></td></tr></tbody></table>


		<table id="1_3" class="hiddenDiv" onclick="showAnswer(this.id,200);" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="middle"><p class="preguntas">
		Expresión que representa un trinomio
		<br>
		<br>
		a) a^3 + b^3 <br><br>	b)3A+3b<br><br>	c)A+B+C<br><br></td></tr></tbody></table>

		<table id="1_3_resp" class="hiddenDiv" onclick="showScore(this.id)" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="middle"><p class="respuestas">A+B+B</p></td></tr></tbody></table>
   

		<table id="2_3" class="hiddenDiv" onclick="showAnswer(this.id,300);" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="middle"><p class="preguntas">
		El resultado de elevar un binomio al cuadrado es
		<br>
		<br>
		a) Un trinomio cuadrado perfecto<br><br>	b)Un binomio de tercer grado<br><br>	c)Una suma de cuadrados<br><br></td></tr></tbody></table>

		<table id="2_3_resp" class="hiddenDiv" onclick="showScore(this.id)" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="middle"><p class="respuestas">Un trinomio cuadrado perfecto</p></td></tr></tbody></table>


		<table id="3_3" class="hiddenDiv" onclick="showAnswer(this.id,400);" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="middle"><p class="preguntas">
		Si un ángulo mide 85° es
		<br>
		<br>
		a)Colineal <br><br>	b)Obtuso<br><br>	c)Agudo<br><br></td></tr></tbody></table>

		<table id="3_3_resp" class="hiddenDiv" onclick="showScore(this.id)" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="middle"><p class="respuestas">Agudo</p></td></tr></tbody></table>


		<table id="4_3" class="hiddenDiv" onclick="showAnswer(this.id,500);" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="middle"><p class="preguntas">
		Si en un triangulo rectángulo, la hipotenusa mide 10 unidades y un cateto 6 unidades, el otro cateto mide
		<br>
		<br>
		a)10 unidades <br><br>	b)8 unidades<br><br>	c)6 unidades<br><br></td></tr></tbody></table>

		<table id="4_3_resp" class="hiddenDiv" onclick="showScore(this.id)" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" valign="middle"><p class="respuestas">8 unidades</p></td></tr></tbody></table>